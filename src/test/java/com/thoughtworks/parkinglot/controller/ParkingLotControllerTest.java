package com.thoughtworks.parkinglot.controller;

import com.google.gson.Gson;
import com.thoughtworks.parkinglot.api.ParkingLotController;
import com.thoughtworks.parkinglot.api.ParkingLotCreateCommand;
import com.thoughtworks.parkinglot.domain.ParkingLotRepository;
import com.thoughtworks.parkinglot.infrastructure.ParkingLotEntity;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class ParkingLotControllerTest {

    private ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);

    private ParkingLotController parkingLotController = new ParkingLotController(parkingLotRepository);


    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(parkingLotController).build();

    @Test
    void should_create_a_new_parking_lot() throws Exception {
        ParkingLotCreateCommand parkingLotCreateCommand = new ParkingLotCreateCommand(
                "环普1号停车场", 300);
        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setId(1);
        parkingLotEntity.setName(parkingLotCreateCommand.name());
        parkingLotEntity.setCapacity(parkingLotCreateCommand.capacity());

        when(parkingLotRepository.save(any())).thenReturn(parkingLotEntity);

        mockMvc.perform(post("/parking-lots")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new Gson().toJson(parkingLotCreateCommand)))
                .andExpect(status().isCreated())
                .andExpect(content().json(new Gson().toJson(parkingLotCreateCommand)))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.capacity").value(300))
                .andExpect(jsonPath("$.name").value("环普1号停车场"));
    }

}
