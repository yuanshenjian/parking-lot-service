package com.thoughtworks.parkinglot.infrastructure;

import com.thoughtworks.parkinglot.domain.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class AccountEntityTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    void should_create_account() {
        AccountEntity account = new AccountEntity();
        account.setName("sjyuan");
        accountRepository.save(account);
        AccountEntity savedAccount = accountRepository.findById(1L).get();
        assertThat(account.getName()).isEqualTo(savedAccount.getName());
    }
}
