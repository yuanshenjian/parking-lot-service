package com.thoughtworks.parkinglot.infrastructure;

import com.thoughtworks.parkinglot.domain.ParkingLotRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@ActiveProfiles("test")
public class ParkingLotEntityTest {

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @Test
    void should_create_account() {
        ParkingLotEntity entity = new ParkingLotEntity();
        entity.setCapacity(300);
        entity.setName("环普P1");
        parkingLotRepository.save(entity);
        ParkingLotEntity savedParkingLot = parkingLotRepository.findById(1L).get();
        assertThat(entity.getName()).isEqualTo(savedParkingLot.getName());
    }
}
