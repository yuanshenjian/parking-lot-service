package com.thoughtworks.parkinglot.api;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ParkingLotApiTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_create_a_new_parking_lot() throws Exception {
        ParkingLotCreateCommand parkingLotCreateCommand = new ParkingLotCreateCommand("环普P1", 300);
        mockMvc.perform(post("/parking-lots")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new Gson().toJson(parkingLotCreateCommand)))
                .andExpect(status().isCreated())
                .andExpect(content().json(new Gson().toJson(parkingLotCreateCommand)))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.capacity").value(300))
                .andExpect(jsonPath("$.name").value("环普P1"));

    }
}
