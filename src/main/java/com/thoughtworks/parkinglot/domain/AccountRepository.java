package com.thoughtworks.parkinglot.domain;

import com.thoughtworks.parkinglot.infrastructure.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, Long>{
}
