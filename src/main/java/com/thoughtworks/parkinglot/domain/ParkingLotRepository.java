package com.thoughtworks.parkinglot.domain;

import com.thoughtworks.parkinglot.infrastructure.ParkingLotEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingLotRepository extends JpaRepository<ParkingLotEntity, Long> {
}
