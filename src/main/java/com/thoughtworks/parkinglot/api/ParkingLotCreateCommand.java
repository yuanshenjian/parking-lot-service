package com.thoughtworks.parkinglot.api;

public record ParkingLotCreateCommand(String name, int capacity) {
}
