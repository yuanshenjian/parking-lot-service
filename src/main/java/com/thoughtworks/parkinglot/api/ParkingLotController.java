package com.thoughtworks.parkinglot.api;

import com.thoughtworks.parkinglot.domain.ParkingLotRepository;
import com.thoughtworks.parkinglot.infrastructure.ParkingLotEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parking-lots")
public class ParkingLotController {
    private ParkingLotRepository parkingLotRepository;

    @Autowired
    public ParkingLotController(ParkingLotRepository parkingLotRepository) {
        this.parkingLotRepository = parkingLotRepository;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody ParkingLotCreateCommand parkingLotCreateCommand) {
        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setName(parkingLotCreateCommand.name());
        parkingLotEntity.setCapacity(parkingLotCreateCommand.capacity());
        ParkingLotEntity createdParkingLot = parkingLotRepository.save(parkingLotEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdParkingLot);
    }
}
