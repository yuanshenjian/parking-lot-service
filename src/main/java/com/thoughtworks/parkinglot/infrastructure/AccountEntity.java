package com.thoughtworks.parkinglot.infrastructure;

import javax.persistence.*;

@Entity
@Table(name = "t_account")
public class AccountEntity {
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
